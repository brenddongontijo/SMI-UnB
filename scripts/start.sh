#!/bin/bash

# Exporting all environment variables to use in crontab
env | sed 's/^\(.*\)$/ \1/g' > /root/env

# Populating database and Initializing cron and gunicorn
python3 manage.py makemigrations                                  && \
python3 manage.py migrate                                         && \
python3 manage.py loaddata src/smi_unb/fixtures/initial_data.json && \
cron                                                              && \
gunicorn smi_unb.wsgi -b 0.0.0.0:8000