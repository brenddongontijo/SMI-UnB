from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render

from smi_unb.api.synchronization import SyncManager
from smi_unb.buildings.models import Building
from .forms import EnergyForm, EnableTransductorForm, DisableTransductorForm
from .models import EnergyTransductor


@login_required
def info(request, transductor_id):
    transductor = get_object_or_404(EnergyTransductor, pk=transductor_id)

    return render(
        request, 'transductor/info.html',
        {
            'campus': transductor.building.campus,
            'building': transductor.building,
            'transductor': transductor
        }
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_transductors"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def new(request, building_id):
    building = get_object_or_404(Building, pk=building_id)

    if request.POST:
        form = EnergyForm(request.POST, building=building)

        if form.is_valid():
            transductor = form.save()

            transductor.synchronized = SyncManager.sync_transductor(
                transductor, new=True)

            transductor.save()

            return HttpResponseRedirect(
                reverse(
                    'buildings:index',
                    kwargs={'building_id': building_id}
                )
            )
    else:
        form = EnergyForm(building=building)

    return render(
        request,
        'transductor/new.html',
        {'form': form, 'campus': building.campus, 'building': building}
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_transductors"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def edit(request, transductor_id):
    transductor = get_object_or_404(EnergyTransductor, pk=transductor_id)

    if request.POST:
        form = EnergyForm(
            request.POST,
            instance=transductor,
            building=transductor.building
        )

        if form.has_changed():
            if form.is_valid():
                transductor = form.save(commit=False)

                transductor.synchronized = SyncManager.sync_transductor(
                    transductor)

                transductor.save()

                return HttpResponseRedirect(
                    reverse(
                        'transductor:info',
                        kwargs={'transductor_id': transductor.id}
                    )
                )
        else:
            form.add_error(None, "Nenhuma Mudança Encontrada.")
    else:
        form = EnergyForm(
            instance=transductor,
            building=transductor.building
        )

    return render(
        request,
        'transductor/edit.html',
        {
            'form': form,
            'building': transductor.building,
            'campus': transductor.building.campus
        }
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_transductors"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def enable(request, transductor_id):
    transductor = get_object_or_404(EnergyTransductor, pk=transductor_id)

    if request.POST:
        form = EnableTransductorForm(request.POST, instance=transductor)

        if form.is_valid():
            transductor = form.save(commit=False)
            transductor.set_transductor_active()
            transductor.synchronized = SyncManager.sync_transductor(
                transductor)
            transductor.save()

            next_page = request.POST.get('next')

            if next_page:
                return HttpResponseRedirect(next_page)

    return HttpResponseRedirect(
        reverse(
            'transductor:info',
            kwargs={'transductor_id': transductor.id}
        )
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_transductors"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def disable(request, transductor_id):
    transductor = get_object_or_404(EnergyTransductor, pk=transductor_id)

    if request.POST:
        form = DisableTransductorForm(request.POST, instance=transductor)

        if form.is_valid():
            transductor = form.save(commit=False)
            transductor.set_transductor_inactive()
            transductor.synchronized = SyncManager.sync_transductor(
                transductor)
            transductor.save()

    return HttpResponseRedirect(
        reverse(
            'transductor:info',
            kwargs={'transductor_id': transductor.id}
        )
    )
