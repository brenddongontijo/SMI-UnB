from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import TestCase
from django.utils import timezone

from smi_unb.buildings.models import Building
from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.transductor.models import EnergyTransductor, TransductorModel, \
                                       EnergyOperations, EnergyMeasurements, \
                                       Measurements


class EnergyTransductorTestCase(TestCase):
    def setUp(self):
        self.building = self.create_building()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

        self.transductor = EnergyTransductor.objects.create(
            building=self.building,
            model=self.t_model,
            name='transductor test',
            ip_address="111.111.111.111"
        )

        self.create_energy_measurements()

    def test_should_not_create_transductor_with_wrong_ip_address(self):
        t_model = self.t_model

        transductor = EnergyTransductor()
        transductor.building = self.building
        transductor.name = 'test'
        transductor.serie_number = "1"
        transductor.creation_date = timezone.now()
        transductor.model = t_model
        transductor.ip_address = "1"

        self.assertRaises(ValidationError, transductor.full_clean)

    def test_should_not_create_transductor_with_same_ip_address(self):
        t_model = self.t_model

        transductor = EnergyTransductor()
        transductor.building = self.building
        transductor.name = 'test'
        transductor.serie_number = "2"
        transductor.creation_date = timezone.now()
        transductor.model = t_model
        transductor.ip_address = "111.111.111.111"

        self.assertRaises(IntegrityError, transductor.save)

    def test_str_from_transductor_model(self):
        t_model = self.t_model

        self.assertEqual(t_model.name, t_model.__str__())

    def test_str_from_energy_transductor(self):
        transductor = self.transductor

        self.assertEqual(transductor.name, transductor.__str__())

    def test_calculate_total_power_and_str_method(self):
        e_measurement = EnergyMeasurements.objects.get(
            voltage_a=122.875,
            voltage_b=122.784,
            voltage_c=121.611
        )

        colection_date = '%s' % e_measurement.collection_date

        self.assertEqual(colection_date, e_measurement.__str__())

        total_active_power = EnergyOperations.calculate_total_power(
            e_measurement.active_power_a,
            e_measurement.active_power_b,
            e_measurement.active_power_c
        )

        self.assertAlmostEqual(
            8.716, total_active_power, places=3, msg=None, delta=None
        )

    def test_calculate_apparent_power(self):
        self.assertEqual(5, EnergyOperations.calculate_apparent_power(3, 4))
        self.assertEqual(13, EnergyOperations.calculate_apparent_power(12, 5))
        self.assertEqual(25, EnergyOperations.calculate_apparent_power(24, 7))

    def test_set_transductor_broken(self):
        self.transductor.set_transductor_broken(True)
        self.assertTrue(self.transductor.broken)

        self.transductor.set_transductor_broken(False)
        self.assertFalse(self.transductor.broken)

    def test_father_method_save_measurements(self):
        measurements = Measurements()
        self.assertEqual(None, measurements.save_measurements('any value'))

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building

    def create_energy_measurements(self):
        for index in range(0, 4):
            e_measurement = EnergyMeasurements()
            e_measurement.transductor = self.transductor

            e_measurement.voltage_a = 122.875 + index
            e_measurement.voltage_b = 122.784 + index
            e_measurement.voltage_c = 121.611 + index

            e_measurement.current_a = 22.831 + index
            e_measurement.current_b = 17.187 + index
            e_measurement.current_c = 3.950 + index

            e_measurement.active_power_a = 2.794 + index
            e_measurement.active_power_b = 1.972 + index
            e_measurement.active_power_c = 3.950 + index

            e_measurement.reactive_power_a = -0.251 - index
            e_measurement.reactive_power_b = -0.752 - index
            e_measurement.reactive_power_c = -1.251 - index

            e_measurement.apparent_power_a = 2.805 + index
            e_measurement.apparent_power_b = 2.110 + index
            e_measurement.apparent_power_c = 4.144 + index

            e_measurement.save()
