import requests

from django import forms
from django.utils import timezone
from django.utils.translation import ugettext as _

from .models import EnergyTransductor, TransductorModel


class EnergyForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        building = kwargs.pop('building')
        super(EnergyForm, self).__init__(*args, **kwargs)

        self.fields['building'].label = "Edifício"
        self.fields['building'].widget = forms.Select(
            attrs={
                'class': 'selectpicker form-control'
            }
        )
        self.fields['building'].empty_label = None
        self.fields['building'].queryset = self.fields['building'] \
                                               .queryset \
                                               .filter(id=building.id)

    class Meta:
        model = EnergyTransductor
        fields = (
            'building', 'model', 'ip_address', 'name', 'serie_number',
            'local_description', 'comments', 'calibration_date'
        )

    model = forms.ModelChoiceField(
        label="Modelo",
        queryset=TransductorModel.objects.all(),
        required=True,
        initial=0,
        widget=forms.Select(
            attrs={
                'class': 'selectpicker form-control',
                'data-live-search': 'true'
            }
        )
    )

    ip_address = forms.CharField(
        label="Número de IP",
        max_length=15,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'ip_address',
                'size': 15,
                'type': 'text'
            }
        )
    )

    name = forms.CharField(
        label="Nome",
        max_length=50,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'name',
                'size': 50,
                'type': 'text'
            }
        )
    )

    serie_number = forms.IntegerField(
        label="Número de Serie",
        required=False,
        widget=forms.NumberInput(
            attrs={
                'class': 'form-control',
                'name': 'serie_number',
                'min': '0', 'max':
                '999999'
            }
        )
    )

    local_description = forms.CharField(
        label="Descrição do Local Instalado",
        max_length=150,
        required=False,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'name': 'description',
                'size': 150,
                'type': 'text'
            }
        )
    )

    comments = forms.CharField(
        label="Comentários",
        max_length=150,
        required=False,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'name': 'description',
                'size': 150,
                'type': 'text'
            }
        )
    )

    calibration_date = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        label="Data Última Calibração",
        widget=forms.DateTimeInput(
            attrs={
                'class': 'form-control',
                'type': 'text',
                'id': 'id_calibration_date'
            }
        ),
        required=False
    )

    def is_valid(self):
        valid = super(EnergyForm, self).is_valid()

        if not valid:
            return valid

        calibration_date = self.cleaned_data['calibration_date']

        if calibration_date:
            now = timezone.now()

            if calibration_date > now:
                self.add_error(
                    'calibration_date',
                    _('Data de calibração não ' +
                      'pode ser depois do horário atual.')
                )

                return False

        url = 'http://' + self.instance.building.server_ip_address
        try:
            request = requests.get(url, timeout=3)
        except:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício.')
            )
            return False

        if request.status_code != 200:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício.')
            )
            return False

        return True


class EnableTransductorForm(forms.ModelForm):
    class Meta:
        model = EnergyTransductor
        fields = []

    def is_valid(self):
        valid = super(EnableTransductorForm, self).is_valid()

        if not valid:
            return valid

        if self.instance.active:
            self.add_error(
                None,
                _('O transdutor já está ativo.')
            )
            return False

        if not self.instance.building.active:
            self.add_error(
                None,
                _('O Edifício do transdutor está desativado. Ative-o antes' +
                  ' de tentar ativar este transdutor.')
            )
            return False

        url = 'http://' + self.instance.building.server_ip_address
        try:
            request = requests.get(url, timeout=3)
        except:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício.')
            )
            return False

        if request.status_code != 200:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício.')
            )
            return False

        return True


class DisableTransductorForm(forms.ModelForm):
    class Meta:
        model = EnergyTransductor
        fields = []

    def is_valid(self):
        valid = super(DisableTransductorForm, self).is_valid()

        if not valid:
            return valid

        if not self.instance.active:
            self.add_error(
                None,
                _('O transdutor já está desativado.')
            )
            return False

        if not self.instance.building.active:
            self.add_error(
                None,
                _('O Edifício do transdutor está desativado. Ative-o antes' +
                  ' de tentar desativar este transdutor.')
            )
            return False

        url = 'http://' + self.instance.building.server_ip_address
        try:
            request = requests.get(url, timeout=3)
        except:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício.')
            )
            return False

        if request.status_code != 200:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício.')
            )
            return False

        return True
