from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils.translation import ugettext as _

from smi_unb.api.synchronization import SyncManager
from smi_unb.campuses.models import Campus
from .forms import BuildingForm, EnableBuildingForm, DisableBuildingForm
from .models import Building


@login_required
def index(request, building_id):
    building = get_object_or_404(Building, pk=building_id)

    transductors_list = building.get_all_active_transductors()
    inactive_transductors = bool(building.get_all_inactive_transductors())

    return render(
        request,
        'buildings/building_index.html',
        {
            'campus': building.campus,
            'building': building,
            'transductors_list': transductors_list,
            'inactive_transductors': inactive_transductors
        }
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_buildings"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def new_building(request, campus_string):
    campus = get_object_or_404(Campus, url_param=campus_string)

    if request.POST:
        form = BuildingForm(request.POST, campus=campus)

        if form.is_valid():
            building = form.save()

            building.synchronized = SyncManager.sync_building(
                building, new=True)

            building.save()

            return HttpResponseRedirect(
                reverse(
                    'campuses:campus_index',
                    kwargs={'campus_string': campus.url_param}
                )
            )
    else:
        form = BuildingForm(
            campus=campus
        )

    return render(
        request,
        'buildings/new_building.html',
        {'campus': campus, 'form': form}
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_buildings"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def edit_building(request, building_id, campus_string):
    building = get_object_or_404(Building, pk=building_id)

    if not building.campus.url_param == campus_string:
        return render(request, '404.html')

    if request.POST:
        form = BuildingForm(
            request.POST,
            instance=building,
            campus=building.campus
        )

        if form.is_valid():
            if form.has_changed():
                building = form.save(commit=False)

                building.synchronized = SyncManager.sync_building(
                    building)

                building.save()

                return HttpResponseRedirect(
                    reverse(
                        'buildings:building_info',
                        kwargs={
                            'building_id': building.id
                        }
                    )
                )
            else:
                form.add_error(None, _("Nenhuma Mudança Encontrada."))
    else:
        form = BuildingForm(instance=building, campus=building.campus)

    return render(
        request,
        'buildings/edit_building.html',
        {'campus': building.campus, 'building': building, 'form': form}
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_buildings"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def enable_building(request, building_id):
    building = get_object_or_404(Building, pk=building_id)

    if request.POST:
        form = EnableBuildingForm(request.POST, instance=building)

        if form.is_valid():
            building = form.save(commit=False)
            building.set_building_active()
            building.synchronized = SyncManager.sync_building(
                building)
            building.save()

            next_page = request.POST.get('next')

            if next_page:
                return HttpResponseRedirect(next_page)

    return HttpResponseRedirect(
        reverse(
            'buildings:building_info',
            kwargs={'building_id': building.id}
        )
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_buildings"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def disable_building(request, building_id):
    building = get_object_or_404(Building, pk=building_id)

    if request.POST:
        form = DisableBuildingForm(request.POST, instance=building)

        if form.is_valid():
            building = form.save(commit=False)
            building.set_building_inactive()
            building.synchronized = SyncManager.sync_building(
                building)
            building.save()

    return HttpResponseRedirect(
        reverse(
            'buildings:building_info',
            kwargs={'building_id': building.id}
        )
    )


@login_required
def building_info(request, building_id):
    building = get_object_or_404(Building, pk=building_id)

    return render(
        request,
        'buildings/building_info.html',
        {
            'campus': building.campus,
            'building': building
        }
    )


@login_required
def building_broken_transductors(request, building_id):
    building = get_object_or_404(Building, pk=building_id)

    broken_transductors = building.get_all_broken_transductors()

    return render(
        request,
        'buildings/building_broken_transductors.html',
        {
            'campus': building.campus,
            'building': building,
            'broken_transductors': broken_transductors
        }
    )


@login_required
@user_passes_test(
    lambda user: user.has_perm("users.manager_transductors"),
    login_url='/painel_controle/',
    redirect_field_name=None)
def inactive_transductors(request, building_id):
    building = get_object_or_404(Building, pk=building_id)
    inactive_transductors = building.get_all_inactive_transductors()

    return render(
        request,
        'buildings/inactive_transductors.html',
        {
            'campus': building.campus,
            'building': building,
            'inactive_transductors': inactive_transductors
        }
    )
