from django.test import TestCase

from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.transductor.models import EnergyTransductor, TransductorModel
from smi_unb.buildings.models import Building


class BuildingsModelsTests(TestCase):
    def setUp(self):
        self.building_active, self.building_inactive = self.create_buildings()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

        self.transductor_1 = EnergyTransductor.objects.create(
            building=self.building_active,
            model=self.t_model,
            name="transductor test 1",
            ip_address="1.1.1.1",
            active=True
        )

        self.transductor_2 = EnergyTransductor.objects.create(
            building=self.building_active,
            model=self.t_model,
            name="transductor test 2",
            ip_address="2.2.2.2",
            active=False
        )

        self.transductor_3 = EnergyTransductor.objects.create(
            building=self.building_active,
            model=self.t_model,
            name="transductor test 3",
            ip_address="3.3.3.3",
            active=True,
            broken=False
        )

        self.transductor_4 = EnergyTransductor.objects.create(
            building=self.building_active,
            model=self.t_model,
            name="transductor test 4",
            ip_address="4.4.4.4",
            active=True,
            broken=True
        )

    def create_buildings(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone"
        )

        building_active = Building.objects.create(
            campus=campus,
            name="Test Building 1",
            server_ip_address="1.1.1.1",
        )

        building_inactive = Building.objects.create(
            campus=campus,
            name="Test Building 2",
            server_ip_address="2.2.2.2",
            active=False
        )

        return [building_active, building_inactive]

    def test_set_building_active(self):
        self.building_active.active = False

        self.building_active.set_building_active()

        self.assertEqual(True, self.building_active.active)

    def test_set_building_inactive(self):
        self.building_active.set_building_inactive()

        self.assertEqual(False, self.building_active.active)

    def test_get_all_active_buildings(self):
        active_buildings = list(Building.objects.get_all_active_buildings())

        self.assertEqual([self.building_active], active_buildings)

    def test_get_all_inactive_buildings(self):
        inactive_buildings = list(
            Building.objects.get_all_inactive_buildings())

        self.assertEqual([self.building_inactive], inactive_buildings)

    def test_str_method(self):
        self.assertEqual(
            self.building_active.name, self.building_active.__str__())

    def test_get_all_working_transductors(self):
        self.assertEqual(
            [self.transductor_3],
            list(self.building_active.get_all_working_transductors())
        )

    def test_get_all_broken_transductors(self):
        self.assertEqual(
            [self.transductor_4, self.transductor_1],
            list(self.building_active.get_all_broken_transductors())
        )

    def test_get_all_active_transductors(self):
        self.assertEqual(
            [self.transductor_4, self.transductor_3, self.transductor_1],
            list(self.building_active.get_all_active_transductors())
        )

    def test_get_all_inactive_transductors(self):
        self.assertEqual(
            [self.transductor_2],
            list(self.building_active.get_all_inactive_transductors())
        )
