import mock
import requests

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test.client import Client
from django.test import TestCase

from smi_unb.api.synchronization import SyncManager
from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.transductor.models import EnergyTransductor, TransductorModel
from smi_unb.users.models import UserPermissions
from smi_unb.buildings.forms import EnableBuildingForm, DisableBuildingForm
from smi_unb.buildings.models import Building


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse(None, 200)


class BuildingsViewsTests(TestCase):
    def setUp(self):
        self.client = Client()

        self.user = User(
            username='test',
            email="test@test.com",
            is_superuser=False,
        )
        self.user.set_password('password')
        self.user.save()

        self.building = self.create_building()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

        self.transductor = EnergyTransductor.objects.create(
            building=self.building,
            model=self.t_model,
            ip_address='1.1.1.1'
        )

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone",
            url_param="test_campus"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building

    '''
    Pages Access Tests
    '''
    def test_buildings_pages_status_code_with_login(self):
        self.client.login(username=self.user.email, password='password')

        url_index = reverse(
            'buildings:index',
            kwargs={'building_id': self.building.id}
        )
        response_1 = self.client.get(url_index)
        self.assertEqual(200, response_1.status_code)

        url_info = reverse(
            'buildings:building_info',
            kwargs={
                'building_id': self.building.id
            }
        )
        response_2 = self.client.get(url_info)
        self.assertEqual(200, response_2.status_code)

        url_broken_transductors = reverse(
            'buildings:building_broken_transductors',
            kwargs={
                'building_id': self.building.id
            }
        )
        response_3 = self.client.get(url_broken_transductors)
        self.assertEqual(200, response_3.status_code)

    def test_buildings_pages_status_code_without_login(self):
        url_index = reverse(
            'buildings:index',
            kwargs={'building_id': self.building.id}
        )
        response_1 = self.client.get(url_index)
        self.assertEqual(302, response_1.status_code)

        url_new = reverse(
            'buildings:new_building',
            kwargs={'campus_string': self.building.campus.url_param}
        )
        response_2 = self.client.get(url_new)
        self.assertEqual(302, response_2.status_code)

        url_edit = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': self.building.campus.url_param
            }
        )
        response_3 = self.client.get(url_edit)
        self.assertEqual(302, response_3.status_code)

        url_info = reverse(
            'buildings:building_info',
            kwargs={
                'building_id': self.building.id
            }
        )
        response_4 = self.client.get(url_info)
        self.assertEqual(302, response_4.status_code)

        url_broken_transductors = reverse(
            'buildings:building_broken_transductors',
            kwargs={
                'building_id': self.building.id
            }
        )
        response_5 = self.client.get(url_broken_transductors)
        self.assertEqual(302, response_5.status_code)

    def test_access_new_and_edit_building_with_permission(self):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url_new = reverse(
            'buildings:new_building',
            kwargs={'campus_string': self.building.campus.url_param}
        )
        response_1 = self.client.get(url_new)
        self.assertEqual(response_1.status_code, 200)

        url_edit = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': self.building.campus.url_param
            }
        )
        response_2 = self.client.get(url_edit)
        self.assertEqual(response_2.status_code, 200)

    def test_not_access_new_and_edit_building_without_permission(self):
        self.client.login(username=self.user.email, password='password')

        url_new = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': self.building.campus.url_param
            }
        )
        response_1 = self.client.get(url_new)
        self.assertRedirects(response_1, reverse('dashboard'))

        url_edit = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': self.building.campus.url_param
            }
        )
        response_2 = self.client.get(url_edit)
        self.assertRedirects(response_2, reverse('dashboard'))

    '''
    Tests Create Building
    '''
    @mock.patch.object(SyncManager, 'sync_building', return_value=True)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_crete_new_building(self, mock_get, mock_sync):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:new_building',
            kwargs={'campus_string': self.building.campus.url_param}
        )

        params = {
            'campus': self.building.campus.id,
            'name': 'Test Building 2',
            'server_ip_address': '2.2.2.2'
        }

        old_building_count = Building.objects.count()

        response = self.client.post(url, params)

        new_building_count = Building.objects.count()

        self.assertEqual(old_building_count + 1, new_building_count)
        self.assertRedirects(
            response,
            reverse(
                'campuses:campus_index',
                kwargs={'campus_string': self.building.campus.url_param}
            )
        )

    @mock.patch.object(SyncManager, 'sync_building', return_value=True)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_not_create_new_building_invalid_params(self, mock_1, mock_2):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:new_building',
            kwargs={'campus_string': self.building.campus.url_param}
        )

        params = {
            'campus': self.building.campus.id,
            'name': 'Test Building 2',
            'server_ip_address': 1234
        }

        old_building_count = Building.objects.count()

        self.client.post(url, params)

        new_building_count = Building.objects.count()

        self.assertEqual(old_building_count, new_building_count)

    @mock.patch('requests.get', side_effect=requests.ConnectionError('Error'))
    def test_not_create_new_building_server_connection_failed(self, mock):
        UserPermissions.add_user_permissions(self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:new_building',
            kwargs={'campus_string': self.building.campus.url_param}
        )

        params = {
            'campus': self.building.campus.id,
            'name': 'Test Building 2',
            'server_ip_address': '2.2.2.2'
        }

        response = self.client.post(url, params)

        error_msg = 'Não foi possível comunicar com o servidor do Edifício.'
        error_msg += ' Verifique o endereço de IP informado.'

        self.assertFormError(response, 'form', None, error_msg)

    '''
    Tests Edit Building
    '''
    @mock.patch.object(SyncManager, 'sync_building', return_value=True)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_edit_building(self, mock_get, mock_sync):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': self.building.campus.url_param
            }
        )

        new_name = 'Edited Name'
        new_ip_address = '4.3.2.1'

        params = {
            'campus': self.building.campus.id,
            'name': new_name,
            'server_ip_address': new_ip_address
        }

        self.client.post(url, params)

        edited_building = Building.objects.get(pk=self.building.id)

        self.assertEqual(new_name, edited_building.name)
        self.assertEqual(new_ip_address, edited_building.server_ip_address)
        self.assertEqual(True, edited_building.synchronized)

    @mock.patch.object(SyncManager, 'sync_building', return_value=True)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_not_edit_building_wrong_params(self, mock_get, mock_sync):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': self.building.campus.url_param
            }
        )

        new_ip_address = '4.3.2.1'

        params = {
            'campus': self.building.campus.id,
            'name': '',
            'server_ip_address': new_ip_address
        }

        response = self.client.post(url, params)
        error_msg = 'This field is required.'
        self.assertFormError(response, 'form', 'name', error_msg)

    @mock.patch('requests.get', side_effect=requests.ConnectionError('Error'))
    def test_not_edit_building_server_connection_failed(self, mock):
        UserPermissions.add_user_permissions(self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': self.building.campus.url_param
            }
        )

        new_name = 'Edited Name'
        new_ip_address = '4.3.2.1'

        params = {
            'campus': self.building.campus.id,
            'name': new_name,
            'server_ip_address': new_ip_address
        }

        response = self.client.post(url, params)

        error_msg = 'Não foi possível comunicar com o servidor do Edifício.'
        error_msg += ' Verifique o endereço de IP informado.'

        self.assertFormError(response, 'form', None, error_msg)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_not_edit_building_without_changes(self, mock):
        UserPermissions.add_user_permissions(self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': self.building.campus.url_param
            }
        )

        params = {
            'campus': self.building.campus.id,
            'name': self.building.name,
            'server_ip_address': self.building.server_ip_address
        }

        response = self.client.post(url, params)
        error_msg = 'Nenhuma Mudança Encontrada.'
        self.assertFormError(response, 'form', None, error_msg)

    def test_not_edit_building_different_campus_string_param(self):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:edit_building',
            kwargs={
                'building_id': self.building.id,
                'campus_string': 'wrong_campus'
            }
        )

        response = self.client.get(url)

        self.assertTemplateUsed(response, '404.html')

    # '''
    # Tests Enable Building
    # '''
    @mock.patch.object(
        Building, 'set_building_active', return_value=True)
    @mock.patch.object(SyncManager, 'sync_building', return_value=True)
    @mock.patch.object(
        EnableBuildingForm, 'is_valid',
        autospec=True, return_value=True)
    def test_enable_building(
        self, mock_is_valid, mock_sync, mock_set_building_active
    ):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:enable_building',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'next': ''
        }

        response = self.client.post(url, params)

        mock_set_building_active.assert_called()
        self.assertRedirects(
            response,
            reverse(
                'buildings:building_info',
                kwargs={'building_id': self.building.id}
            )
        )

    @mock.patch.object(
        Building, 'set_building_active', return_value=True)
    @mock.patch.object(SyncManager, 'sync_building', return_value=True)
    @mock.patch.object(
        EnableBuildingForm, 'is_valid',
        autospec=True, return_value=True)
    def test_enable_building_with_next_page_param(
        self, mock_is_valid, mock_sync, mock_set_building_active
    ):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:enable_building',
            kwargs={'building_id': self.building.id}
        )

        next_page = reverse('dashboard')

        params = {
            'next': next_page
        }

        response = self.client.post(url, params)

        mock_set_building_active.assert_called()
        self.assertRedirects(
            response,
            next_page
        )

    @mock.patch.object(
        Building, 'set_building_active', return_value=True)
    @mock.patch.object(
        EnableBuildingForm, 'is_valid',
        autospec=True, return_value=True)
    def test_not_enable_building_without_permission(
        self, mock_is_valid, mock_set_building_active
    ):
        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:enable_building',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'next': ''
        }

        response = self.client.post(url, params)

        mock_set_building_active.assert_not_called()
        self.assertRedirects(response, reverse('dashboard'))

    @mock.patch.object(
        Building, 'set_building_active', return_value=True)
    @mock.patch.object(
        EnableBuildingForm, 'is_valid',
        autospec=True, return_value=False)
    def test_not_enable_building_invalid_form(
        self, mock_is_valid, mock_set_building_active
    ):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:enable_building',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'next': ''
        }

        response = self.client.post(url, params)

        mock_set_building_active.assert_not_called()
        self.assertRedirects(
            response,
            reverse(
                'buildings:building_info',
                kwargs={'building_id': self.building.id}
            )
        )

    @mock.patch.object(
        Building, 'set_building_active', return_value=True)
    def test_not_enable_building_get_method(self, mock_set_building_active):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:enable_building',
            kwargs={'building_id': self.building.id}
        )

        response = self.client.get(url)

        mock_set_building_active.assert_not_called()
        self.assertRedirects(
            response,
            reverse(
                'buildings:building_info',
                kwargs={'building_id': self.building.id}
            )
        )

    # '''
    # Tests Disable Building
    # '''
    @mock.patch.object(SyncManager, 'sync_building', return_value=True)
    @mock.patch.object(
        DisableBuildingForm, 'is_valid',
        autospec=True, return_value=True)
    def test_disable_building(self, mock_is_valid, mock_sync):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:disable_building',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'next': ''
        }

        response = self.client.post(url, params)

        mock_sync.assert_called()
        self.assertRedirects(
            response,
            reverse(
                'buildings:building_info',
                kwargs={'building_id': self.building.id}
            )
        )

    @mock.patch.object(
        Building, 'set_building_inactive', return_value=True)
    @mock.patch.object(
        DisableBuildingForm, 'is_valid',
        autospec=True, return_value=True)
    def test_not_disable_building_without_permission(
        self, mock_is_valid, mock_set_building_inactive
    ):
        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:disable_building',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'next': ''
        }

        response = self.client.post(url, params)

        mock_set_building_inactive.assert_not_called()
        self.assertRedirects(response, reverse('dashboard'))

    @mock.patch.object(
        Building, 'set_building_inactive', return_value=True)
    @mock.patch.object(
        DisableBuildingForm, 'is_valid',
        autospec=True, return_value=False)
    def test_not_disable_building_invalid_form(
        self, mock_is_valid, mock_set_building_inactive
    ):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:disable_building',
            kwargs={'building_id': self.building.id}
        )

        params = {
            'next': ''
        }

        response = self.client.post(url, params)

        mock_set_building_inactive.assert_not_called()
        self.assertRedirects(
            response,
            reverse(
                'buildings:building_info',
                kwargs={'building_id': self.building.id}
            )
        )

    @mock.patch.object(
        Building, 'set_building_inactive', return_value=True)
    def test_not_disable_building_with_get_method(
        self, mock_set_building_inactive
    ):
        UserPermissions.add_user_permissions(
            self.user, ['manager_buildings'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:disable_building',
            kwargs={'building_id': self.building.id}
        )

        response = self.client.get(url)

        mock_set_building_inactive.assert_not_called()
        self.assertRedirects(
            response,
            reverse(
                'buildings:building_info',
                kwargs={'building_id': self.building.id}
            )
        )

    # '''
    # Tests Inactive Transductors
    # '''
    @mock.patch.object(
        Building, 'get_all_inactive_transductors',
        return_value=[])
    def test_inactive_transductors(self, mock_method):
        UserPermissions.add_user_permissions(
            self.user, ['manager_transductors'])

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:inactive_transductors',
            kwargs={'building_id': self.building.id}
        )

        response = self.client.get(url)

        mock_method.assert_called()
        self.assertIn(
            'Nenhum Transdutor Desativado', response.content.decode('utf-8'))

    def test_inactive_transductors_without_user_permission(self):

        self.client.login(username=self.user.email, password='password')

        url = reverse(
            'buildings:inactive_transductors',
            kwargs={'building_id': self.building.id}
        )

        response = self.client.get(url)

        self.assertRedirects(response, reverse('dashboard'))
