import requests

from django import forms
from django.utils.translation import ugettext as _

from .models import Building


class BuildingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        campus = kwargs.pop('campus')
        super(BuildingForm, self).__init__(*args, **kwargs)
        self.fields['campus'].label = "Campus"
        self.fields['campus'].widget = forms.Select(
            attrs={
                'class': 'selectpicker form-control'
            }
        )
        self.fields['campus'].empty_label = None
        self.fields['campus'].queryset = self.fields['campus'] \
                                             .queryset.filter(id=campus.id)

    class Meta:
        model = Building
        fields = (
            'campus', 'name', 'acronym', 'server_ip_address', 'description',
            'phone', 'website_address'
        )

    name = forms.CharField(
        label="Nome",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'name',
                'size': 50,
                'type': 'text'
            }
        )
    )

    acronym = forms.CharField(
        label="Sigla",
        max_length=10,
        required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'acronym',
                'size': 10,
                'type': 'text'
            }
        )
    )

    server_ip_address = forms.CharField(
        label="Endereço de IP do Servidor",
        max_length=15,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'server_ip_address',
                'size': 10,
                'type': 'text'
            }
        )
    )

    description = forms.CharField(
        label="Descrição",
        max_length=150,
        required=False,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'name': 'description',
                'size': 150,
                'type': 'text'
            }
        )
    )

    phone = forms.CharField(
        label="Telefone",
        max_length=16,
        required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'phone',
                'size': 16,
                'type': 'text'
            }
        )
    )

    website_address = forms.CharField(
        label="URL do Site",
        max_length=100,
        required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'website_address',
                'size': 100,
                'type': 'text'
            }
        )
    )

    def is_valid(self):
        valid = super(BuildingForm, self).is_valid()

        if not valid:
            return valid

        url = 'http://' + self.instance.server_ip_address
        try:
            request = requests.get(url, timeout=3)
        except:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício. ' +
                  'Verifique o endereço de IP informado.')
            )
            return False

        if request.status_code != 200:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício. ' +
                  'Verifique o endereço de IP informado.')
            )
            return False

        return True


class EnableBuildingForm(forms.ModelForm):
    class Meta:
        model = Building
        fields = []

    def is_valid(self):
        valid = super(EnableBuildingForm, self).is_valid()

        if not valid:
            return valid

        if self.instance.active:
            return False

        url = 'http://' + self.instance.server_ip_address
        try:
            request = requests.get(url, timeout=3)
        except:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício. ' +
                  'Verifique o endereço de IP informado.')
            )
            return False

        if request.status_code != 200:
            self.add_error(
                None,
                _('Não foi possível comunicar com o servidor do Edifício. ' +
                  'Verifique o endereço de IP informado.')
            )
            return False

        return True


class DisableBuildingForm(forms.ModelForm):
    class Meta:
        model = Building
        fields = []

    def is_valid(self):
        valid = super(DisableBuildingForm, self).is_valid()

        if not valid:
            return valid

        if not self.instance.active:
            return False

        return True
