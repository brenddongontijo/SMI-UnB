from django.core.validators import RegexValidator
from django.db import models

from smi_unb.campuses.models import Campus


class BuildingManager(models.Manager):
    def get_all_active_buildings(self):
        return self.get_queryset().filter(active=True)

    def get_all_inactive_buildings(self):
        return self.get_queryset().filter(active=False)


class Building(models.Model):
    campus = models.ForeignKey(Campus)
    name = models.CharField(max_length=50)
    acronym = models.CharField(max_length=10, blank=True)
    description = models.CharField(max_length=150, blank=True)
    phone = models.CharField(max_length=16, blank=True)
    website_address = models.CharField(max_length=100, blank=True)
    server_ip_address = models.CharField(
        max_length=15,
        unique=True,
        validators=[
            RegexValidator(
                regex='^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$',
                message='Incorrect IP address format',
                code='invalid_ip_address'
            ),
        ]
    )
    active = models.BooleanField(default=True)
    synchronized = models.BooleanField(default=False)
    objects = BuildingManager()

    def __str__(self):
        return self.name

    def get_all_working_transductors(self):
        return self.transductor_set.filter(broken=False, active=True)

    def get_all_broken_transductors(self):
        return self.transductor_set.filter(broken=True, active=True)

    def get_all_active_transductors(self):
        return self.transductor_set.filter(active=True)

    def get_all_inactive_transductors(self):
        return self.transductor_set.filter(active=False)

    def set_building_active(self):
        self.active = True

    def set_building_inactive(self):
        self.active = False

    def update_transductors_sent(self, id_transductors_to_update):
        transductors_updated = []

        for id_transductor in id_transductors_to_update:
            transductor = self.transductor_set.get(id=id_transductor)
            transductor.update_last_measurement_sent()

            transductors_updated.append(transductor)

        return transductors_updated

    def api_view_set(self):
        return 'building'
