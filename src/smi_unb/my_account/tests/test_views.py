from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase, Client


class TestUsersViews(TestCase):
    def setUp(self):
        self.client = Client()

        self.superuser = User(
            username='superuser',
            first_name='super',
            last_name='user',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

    def test_getting_status_code_from_pages_with_login(self):
        self.client.login(username=self.superuser.email, password='12345')

        response_1 = self.client.get(reverse('my_account:index'))
        self.assertEqual(200, response_1.status_code)

        response_2 = self.client.get(reverse('my_account:self_edit'))
        self.assertEqual(200, response_2.status_code)

        response_3 = self.client.get(reverse('my_account:change_password'))
        self.assertEqual(200, response_3.status_code)

    def test_getting_status_code_from_pages_without_login(self):
        response_1 = self.client.get(reverse('my_account:index'))
        self.assertEqual(302, response_1.status_code)

        response_2 = self.client.get(reverse('my_account:self_edit'))
        self.assertEqual(302, response_2.status_code)

        response_3 = self.client.get(reverse('my_account:change_password'))
        self.assertEqual(302, response_3.status_code)

    def test_change_password(self):
        self.client.login(username=self.superuser.email, password='12345')

        url = reverse('my_account:change_password')

        new_password = 'newpassword'

        params = {
            'old_password': '12345',
            'new_password1': new_password,
            'new_password2': new_password,
        }

        response = self.client.post(url, params)
        user = User.objects.get(username=self.superuser.username)

        self.assertRedirects(response, reverse('my_account:index'))
        self.assertTrue(check_password(new_password, user.password))

    def test_not_change_password_with_incorrect_old_password(self):
        self.client.login(username=self.superuser.email, password='12345')

        url = reverse('my_account:change_password')

        new_password = 'newpassword'

        params = {
            'old_password': 'wrongpassword',
            'new_password1': new_password,
            'new_password2': new_password,
        }

        response = self.client.post(url, params)
        error_msg = 'Your old password was entered incorrectly.' + \
                    ' Please enter it again.'
        self.assertFormError(response, 'form', 'old_password', error_msg)

    def test_not_change_password_with_incorrect_new_passwords(self):
        self.client.login(username=self.superuser.email, password='12345')

        url = reverse('my_account:change_password')

        params = {
            'old_password': '12345',
            'new_password1': 'new_password',
            'new_password2': 'wrongpassword',
        }

        response = self.client.post(url, params)

        error_msg = "The two password fields didn't match."
        self.assertFormError(response, 'form', 'new_password2', error_msg)

    def test_self_edit_user(self):
        self.client.login(username=self.superuser.email, password='12345')

        url = reverse('my_account:self_edit')

        params = {
            'first_name': 'newfirst',
            'last_name': 'newlast'
        }

        response = self.client.post(url, params)

        self.assertRedirects(response, reverse('my_account:index'))

    def test_not_self_edit_user_without_changes(self):
        self.client.login(username=self.superuser.email, password='12345')

        url = reverse('my_account:self_edit')

        params = {
            'first_name': self.superuser.first_name,
            'last_name': self.superuser.last_name
        }

        response = self.client.post(url, params)

        error_msg = "Nenhuma Mudança Encontrada."
        self.assertFormError(response, 'form', None, error_msg)

    def test_not_self_edit_user_with_first_name_too_long(self):
        self.client.login(username=self.superuser.email, password='12345')

        url = reverse('my_account:self_edit')

        long_name = 'thisfirstnameistotoolongtofitthefield'

        params = {
            'first_name': long_name,
            'last_name': self.superuser.last_name
        }

        response = self.client.post(url, params)

        error_msg = "Ensure this value has at most 30 characters (it has 37)."
        self.assertFormError(response, 'form', 'first_name', error_msg)
