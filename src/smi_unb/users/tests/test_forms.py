from django.contrib.auth.models import User
from django.test import TestCase

from smi_unb.users.forms import NewUserForm, EditUserForm


class UsersFormTest(TestCase):
    def setUp(self):
        self.superuser = User(
            username='test',
            first_name='superfirst',
            last_name='superlast',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

        self.user = User(
            username='normaluser',
            first_name='normalfirst',
            last_name='normallast',
            email="test@test.com",
            is_superuser=False,
        )
        self.user.set_password('12345')
        self.user.save()

    def test_correct_new_user_form(self):
        data = {
            'username': 'superuser',
            'first_name': 'testfirst',
            'last_name': 'testlast',
            'email': 'testemail@test.com',
            'password': 'password',
            'password_confirmation': 'password',
            'user_type': 'adm',
            'permissions_codenames': ['manager_buildings']
        }

        form = NewUserForm(instance=self.superuser, data=data)
        self.assertTrue(form.is_valid())

    def test_new_user_form_email_already_used(self):
        data = {
            'username': 'superuser',
            'first_name': 'testfirst',
            'last_name': 'testlast',
            'email': 'test@test.com',
            'password': 'password',
            'password_confirmation': 'password',
            'user_type': 'adm',
            'permissions_codenames': ['manager_buildings']
        }

        form = NewUserForm(instance=self.superuser, data=data)
        self.assertFalse(form.is_valid())

    def test_new_user_wrong_email_type(self):
        data = {
            'username': 'superuser',
            'first_name': 'testfirst',
            'last_name': 'testlast',
            'email': 'wrongemail',
            'password': 'password',
            'password_confirmation': 'password',
            'user_type': 'adm',
            'permissions_codenames': ['manager_buildings']
        }

        form = NewUserForm(instance=self.superuser, data=data)
        self.assertFalse(form.is_valid())

    def test_new_user_form_different_password_fields(self):
        data = {
            'username': 'superuser',
            'first_name': 'testfirst',
            'last_name': 'testlast',
            'email': 'testemail@test.com',
            'password': 'password',
            'password_confirmation': 'wrongpassword',
            'user_type': 'adm',
            'permissions_codenames': ['manager_buildings']
        }

        form = NewUserForm(instance=self.superuser, data=data)
        self.assertFalse(form.is_valid())

    def test_correct_edit_user_form(self):
        data = {
            'username': 'superuser',
            'first_name': 'testfirst',
            'last_name': 'testlast',
            'email': 'testemail@test.com',
            'user_type': 'adm',
            'permissions_codenames': ['manager_buildings']
        }

        form = EditUserForm(instance=self.superuser, data=data)
        self.assertTrue(form.is_valid())

    def test_edit_user_email_already_used(self):
        data = {
            'username': 'superuser',
            'first_name': 'testfirst',
            'last_name': 'testlast',
            'email': 'test@test.com',
            'user_type': 'adm',
            'permissions_codenames': ['manager_buildings']
        }

        form = EditUserForm(instance=self.superuser, data=data)
        self.assertFalse(form.is_valid())

    def test_edit_user_username_already_used(self):
        data = {
            'username': 'normaluser',
            'first_name': 'testfirst',
            'last_name': 'testlast',
            'email': 'testemail@test.com',
            'user_type': 'adm',
            'permissions_codenames': ['manager_buildings']
        }

        form = EditUserForm(instance=self.superuser, data=data)
        self.assertFalse(form.is_valid())
