import mock

from django.contrib.auth.models import User, Permission
from django.core.urlresolvers import reverse
from django.test import TestCase, Client

from smi_unb.users.forms import DeleteUserForm


class TestUsersViews(TestCase):
    def setUp(self):
        self.client = Client()

        self.superuser = User(
            username='superuser',
            first_name='super',
            last_name='user',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

        self.normaluser = User(
            username='normaluser',
            first_name='normal',
            last_name='user',
            email="test@test.com",
            is_superuser=False,
        )
        self.normaluser.set_password('12345')
        self.normaluser.save()

    def create_user(
        self, username, first_name, last_name,
        password, is_superuser, email, permissions_codenames
    ):
        new_user = User()
        new_user.username = username
        new_user.first_name = first_name
        new_user.last_name = last_name
        new_user.set_password = password
        new_user.is_superuser = is_superuser
        new_user.email = email
        new_user.save()

        for codename in permissions_codenames:
            permission = Permission.objects.get(codename=codename)
            new_user.user_permissions.add(permission)

        return new_user

    def test_getting_status_code_from_pages_with_login(self):
        self.client.login(username=self.superuser.email, password='12345')

        response_1 = self.client.get(reverse('users:index'))
        self.assertEqual(200, response_1.status_code)

        response_2 = self.client.get(reverse('users:new_user'))
        self.assertEqual(200, response_2.status_code)

        response_3 = self.client.get(
            reverse(
                'users:edit_user',
                kwargs={'user_id': self.superuser.id}
            )
        )
        self.assertEqual(200, response_3.status_code)

        response_4 = self.client.get(
            reverse(
                'users:info_user',
                kwargs={'user_id': self.superuser.id}
            )
        )
        self.assertEqual(200, response_4.status_code)

        response_5 = self.client.get(reverse('users:logging_list'))
        self.assertEqual(200, response_5.status_code)

    def test_getting_status_code_from_pages_without_login(self):
        response_1 = self.client.get(reverse('users:index'))
        self.assertEqual(302, response_1.status_code)

        response_2 = self.client.get(reverse('users:new_user'))
        self.assertEqual(302, response_2.status_code)

        response_3 = self.client.get(
            reverse(
                'users:edit_user',
                kwargs={'user_id': self.superuser.id}
            )
        )
        self.assertEqual(302, response_3.status_code)

        response_4 = self.client.get(
            reverse(
                'users:info_user',
                kwargs={'user_id': self.superuser.id}
            )
        )
        self.assertEqual(302, response_4.status_code)

        response_5 = self.client.get(reverse('users:logging_list'))
        self.assertEqual(302, response_5.status_code)

    def test_dont_allow_normal_users_to_use_admin_only_pages(self):
        self.client.login(username=self.normaluser.email, password='12345')

        dashboard_url = reverse('dashboard')

        response_1 = self.client.get(reverse('users:index'))
        self.assertRedirects(response_1, dashboard_url)

        response_2 = self.client.get(reverse('users:new_user'))
        self.assertRedirects(response_2, dashboard_url)

        response_3 = self.client.get(
            reverse(
                'users:edit_user',
                kwargs={'user_id': self.normaluser.id}
            )
        )
        self.assertRedirects(response_3, dashboard_url)

        response_4 = self.client.get(
            reverse(
                'users:info_user',
                kwargs={'user_id': self.normaluser.id}
            )
        )
        self.assertRedirects(response_4, dashboard_url)

        response_5 = self.client.get(
            reverse(
                'users:delete_user',
                kwargs={'user_id': self.normaluser.id}
            )
        )
        self.assertRedirects(response_5, dashboard_url)

    @mock.patch(
        'smi_unb.users.models.UserPermissions.add_user_permissions',
        side_effect=(lambda x, y: True))
    def test_register_new_normal_user(self, mock_1):
        self.client.login(username=self.superuser.email, password='12345')

        register_user_url = reverse('users:new_user')

        params = {
            'username': 'test',
            'first_name': 'lasttest',
            'last_name': 'firsttest',
            'email': 'test@email.com',
            'password': 'password',
            'password_confirmation': 'password',
            'user_type': 'normal',
            'permissions_codenames': {
                'manager_buildings', 'manager_transductors'
            }
        }

        old_total_users = User.objects.count()
        response = self.client.post(register_user_url, params)

        new_normal_user = User.objects.get(username='test')
        new_total_users = User.objects.count()

        mock_1.assert_called()
        self.assertFalse(new_normal_user.is_superuser)
        self.assertEqual(new_total_users - 1, old_total_users)
        self.assertRedirects(response, reverse('users:index'))

    def test_register_new_super_user(self):
        self.client.login(username=self.superuser.email, password='12345')

        register_user_url = reverse('users:new_user')

        params = {
            'username': 'test',
            'first_name': 'lasttest',
            'last_name': 'firsttest',
            'email': 'test@email.com',
            'password': 'password',
            'password_confirmation': 'password',
            'user_type': 'adm',
        }

        old_total_users = User.objects.count()
        response = self.client.post(register_user_url, params)

        new_superuser = User.objects.get(username='test')
        new_total_users = User.objects.count()

        self.assertTrue(new_superuser.is_superuser)
        self.assertEqual(new_total_users - 1, old_total_users)
        self.assertRedirects(response, reverse('users:index'))

    def test_not_register_new_user_with_email_already_used(self):
        self.client.login(username=self.superuser.email, password='12345')

        register_user_url = reverse('users:new_user')

        params = {
            'username': 'test',
            'first_name': 'lasttest',
            'last_name': 'firsttest',
            'email': self.normaluser.email,
            'password': 'password',
            'password_confirmation': 'password',
            'user_type': 'normal',
            'permissions_codenames': {
                'manager_buildings', 'manager_transductors'
            }
        }

        response = self.client.post(register_user_url, params)

        error_msg = 'Email já está sendo usado.'
        self.assertFormError(response, 'form', 'email', error_msg)

    def test_not_register_new_user_with_password_fields_not_matching(self):
        self.client.login(username=self.superuser.email, password='12345')

        register_user_url = reverse('users:new_user')

        params = {
            'username': 'test',
            'first_name': 'lasttest',
            'last_name': 'firsttest',
            'email': 'test@email.com',
            'password': 'password',
            'password_confirmation': 'anotherpassword',
            'user_type': 'normal',
            'permissions_codenames': {
                'manager_buildings', 'manager_transductors'
            }
        }

        response = self.client.post(register_user_url, params)

        error_msg = 'Os dois campos de senha não combinam.'
        self.assertFormError(response, 'form', None, error_msg)

    def test_edit_user(self):
        permissions = ['manager_buildings', 'manager_transductors']

        user = self.create_user(
            'username', 'first', 'last', 'password',
            False, 'email@test.com', permissions
        )

        self.client.login(username=self.superuser.email, password='12345')

        edit_user_url = reverse('users:edit_user', kwargs={'user_id': user.id})

        params = {
            'username': 'another',
            'first_name': 'first',
            'last_name': 'last',
            'email': 'another@email.com',
            'user_type': 'adm'
        }

        self.client.post(edit_user_url, params)

        edited_user = User.objects.get(username='another')

        self.assertEqual('first', edited_user.first_name)
        self.assertEqual('last', edited_user.last_name)
        self.assertEqual('another@email.com', edited_user.email)
        self.assertTrue(edited_user.is_superuser)

    @mock.patch(
        'smi_unb.users.models.UserPermissions.add_user_permissions',
        side_effect=(lambda x, y: True))
    @mock.patch(
        'smi_unb.users.models.UserPermissions.remove_user_permissions',
        side_effect=(lambda x, y: True))
    def test_edit_user_changing_user_permissions(self, mock_1, mock_2):
        user = self.create_user(
            'username',
            'first',
            'last',
            'password',
            True,
            'email@test.com',
            ['manager_transductors']
        )

        self.client.login(username=self.superuser.email, password='12345')

        edit_user_url = reverse('users:edit_user', kwargs={'user_id': user.id})

        params = {
            'username': 'username',
            'first_name': 'first',
            'last_name': 'last',
            'email': 'email@test.com',
            'user_type': 'normal',
            'permissions_codenames': {'manager_buildings'}
        }

        self.client.post(edit_user_url, params)

        edited_user = User.objects.get(username='username')

        mock_1.assert_called()
        mock_2.assert_called()
        self.assertFalse(edited_user.is_superuser)

    @mock.patch(
        'smi_unb.users.models.UserPermissions.remove_all_user_permissions',
        side_effect=(lambda x: True))
    def test_edit_user_remove_all_user_permissions(self, mock_1):
        user = self.create_user(
            'username',
            'first',
            'last',
            'password',
            True,
            'email@test.com',
            ['manager_buildings']
        )

        self.client.login(username=self.superuser.email, password='12345')

        edit_user_url = reverse('users:edit_user', kwargs={'user_id': user.id})

        params = {
            'username': 'username',
            'first_name': 'first',
            'last_name': 'last',
            'email': 'email@test.com',
            'user_type': 'normal',
        }

        self.client.post(edit_user_url, params)

        mock_1.assert_called()

    def test_not_edit_user_without_changes(self):
        user = self.create_user(
            'username',
            'first',
            'last',
            'password',
            True,
            'email@test.com',
            ['manager_buildings']
        )

        self.client.login(username=self.superuser.email, password='12345')

        edit_user_url = reverse('users:edit_user', kwargs={'user_id': user.id})

        params = {
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'user_type': 'adm',
            'permissions_codenames': {'manager_buildings'}
        }

        response = self.client.post(edit_user_url, params)

        error_msg = 'Nenhuma Mudança Encontrada.'
        self.assertFormError(response, 'form', None, error_msg)

    def test_not_edit_user_email_already_used(self):
        user = self.create_user(
            'username',
            'first',
            'last',
            'password',
            False,
            'email@test.com',
            ['manager_buildings', 'manager_transductors']
        )

        self.client.login(username=self.superuser.email, password='12345')

        edit_user_url = reverse('users:edit_user', kwargs={'user_id': user.id})

        params = {
            'username': 'another',
            'first_name': 'first',
            'last_name': 'last',
            'email': self.normaluser.email,
            'user_type': 'adm',
            'permissions_codenames': {
                'manager_buildings', 'manager_transductors'
            }
        }

        response = self.client.post(edit_user_url, params)
        error_msg = 'Email já está sendo usado.'
        self.assertFormError(response, 'form', 'email', error_msg)

    def test_not_delete_user_with_get_request(self):
        self.client.login(username=self.superuser.email, password='12345')

        new_user = self.create_user(
            'test',
            'first',
            'last',
            'password',
            False,
            'test@test.com',
            []
        )

        users_count = User.objects.count()

        url = reverse('users:delete_user', kwargs={'user_id': new_user.id})
        self.client.get(url)

        self.assertEqual(users_count, User.objects.count())

    def test_delete_user_with_post_request(self):
        self.client.login(username=self.superuser.email, password='12345')

        new_user = self.create_user(
            'test',
            'first',
            'last',
            'password',
            False,
            'test@test.com',
            []
        )

        users_count = User.objects.count()

        url = reverse('users:delete_user', kwargs={'user_id': new_user.id})

        params = {
            'delete': ''
        }

        self.client.post(url, params)

        self.assertEqual(users_count - 1, User.objects.count())

    @mock.patch.object(
        DeleteUserForm, 'is_valid', return_value=False, autospec=True)
    def test_not_delete_user_with_wrong_params(self, mock_is_valid):
        self.client.login(username=self.superuser.email, password='12345')

        new_user = self.create_user(
            'test',
            'first',
            'last',
            'password',
            False,
            'test@test.com',
            []
        )

        users_count = User.objects.count()

        url = reverse('users:delete_user', kwargs={'user_id': new_user.id})

        params = {
            'delete': ''
        }

        response = self.client.post(url, params)

        self.assertEqual(users_count, User.objects.count())
        self.assertRedirects(response, reverse('users:index'))
