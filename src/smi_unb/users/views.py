import os

from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User, Permission
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from .forms import NewUserForm, EditUserForm, DeleteUserForm
from .models import UserPermissions


@login_required
@user_passes_test(
    lambda user: user.is_superuser,
    login_url='/painel_controle/',
    redirect_field_name=None)
def index(request):
    users = User.objects.all()

    return render(request, 'users/index.html', {'users': users})


@login_required
@user_passes_test(
    lambda user: user.is_superuser,
    login_url='/painel_controle/',
    redirect_field_name=None)
def info_user(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    user_permissions = Permission.objects.filter(user=user)

    return render(
        request,
        'users/info.html',
        {'user': user, 'user_permissions': user_permissions}
    )


@login_required
@user_passes_test(
    lambda user: user.is_superuser,
    login_url='/painel_controle/',
    redirect_field_name=None)
def new_user(request):
    if request.POST:
        form = NewUserForm(request.POST)

        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(form.cleaned_data['password'])

            user_type = form.cleaned_data['user_type']

            if user_type == 'adm':
                user.is_superuser = True
                user.save()

                UserPermissions.add_all_user_permissions(user)
            else:
                user.is_superuser = False
                user.save()

                permissions_codenames = form.cleaned_data[
                    'permissions_codenames'
                ]

                if permissions_codenames:
                    UserPermissions.add_user_permissions(
                        user, permissions_codenames)

            messages.success(
                request,
                'Usuário ' + str(user.first_name) + ' criado com sucesso!'
            )

            return HttpResponseRedirect(reverse('users:index'))
    else:
        form = NewUserForm()

    return render(request, 'users/new_user.html', {'form': form})


@login_required
@user_passes_test(
    lambda user: user.is_superuser,
    login_url='/painel_controle/',
    redirect_field_name=None)
def edit_user(request, user_id):
    old_user = get_object_or_404(User, pk=user_id)
    user_type = 'adm' if old_user.is_superuser else 'normal'
    user_permissions = [
        (permission.codename) for
        permission in Permission.objects.filter(user=old_user)
    ]

    if request.POST:
        form = EditUserForm(
            request.POST,
            instance=old_user,
            initial={
                'user_type': user_type,
                'permissions_codenames': user_permissions
            }
        )

        if form.is_valid():
            if form.has_changed():
                user = form.save(commit=False)

                if form.cleaned_data['user_type'] == 'adm':
                    user.is_superuser = True

                    UserPermissions.add_all_user_permissions(user)
                else:
                    user.is_superuser = False

                    permissions_changed = form.cleaned_data[
                        'permissions_codenames'
                    ]

                    if permissions_changed:
                        permissions_to_add = list(
                            set(permissions_changed) - set(user_permissions)
                        )
                        permissions_to_remove = list(
                            set(user_permissions) - set(permissions_changed)
                        )

                        UserPermissions.add_user_permissions(
                            user, permissions_to_add)
                        UserPermissions.remove_user_permissions(
                            user, permissions_to_remove)
                    else:
                        UserPermissions.remove_all_user_permissions(user)

                user.save()

                messages.success(
                    request,
                    'Usuário ' + str(user.first_name) + ' editado com sucesso!'
                )
                return HttpResponseRedirect(
                    reverse('users:info_user', kwargs={'user_id': user.id})
                )
            else:
                form.add_error(None, "Nenhuma Mudança Encontrada.")
    else:
        form = EditUserForm(
            instance=old_user,
            initial={
                'user_type': user_type,
                'permissions_codenames': user_permissions
            }
        )

    return render(
        request,
        'users/edit_user.html',
        {'form': form, 'user': old_user}
    )


@login_required
@user_passes_test(
    lambda user: user.is_superuser,
    login_url='/painel_controle/',
    redirect_field_name=None)
def delete_user(request, user_id):
    user = get_object_or_404(User, pk=user_id)

    if request.POST:
        form = DeleteUserForm(request.POST, instance=user)

        if form.is_valid():
            user.delete()

    return HttpResponseRedirect(reverse('users:index'))


@login_required
@user_passes_test(
    lambda user: user.is_superuser,
    login_url='/painel_controle/',
    redirect_field_name=None)
def logging_list(request):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file = open(BASE_DIR + '/logging.logging', 'r')
    file_contentes = file.read()

    return render(request, 'users/logging_list.html',
                  {'logging': file_contentes})
