from django import forms
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import User


class NewUserForm(forms.ModelForm):
    USER_TYPES = [('normal', 'Normal'), ('adm', 'Administrador')]

    USER_PERMISSIONS = (
        ("manager_buildings", "Gerenciar Edifícios"),
        ("manager_transductors", "Gerenciar Transdutores"),
    )

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password', ]

    username = forms.CharField(
        label="Nome de Usuário",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'username',
                'size': 30,
                'type': 'text'
            }
        )
    )

    first_name = forms.CharField(
        label="Nome",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'first_name',
                'size': 30,
                'type': 'text'
            }
        )
    )

    last_name = forms.CharField(
        label="Sobrenome",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'last_name',
                'size': 30,
                'type': 'text'
            }
        )
    )

    email = forms.CharField(
        label="E-mail",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'email',
                'size': 30,
                'type': 'text'
            }
        )
    )

    password = forms.CharField(
        label="Senha",
        max_length=16,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'password',
                'size': 16,
                'type': 'password'
            }
        )
    )

    password_confirmation = forms.CharField(
        label="Confirmação Senha",
        max_length=16,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'password_confirmation',
                'size': 16,
                'type': 'password'
            }
        )
    )

    user_type = forms.ChoiceField(
        label="Tipo de Usuário",
        choices=USER_TYPES,
        widget=forms.RadioSelect,
        initial='normal'
    )

    permissions_codenames = forms.MultipleChoiceField(
        label="Permissões",
        widget=forms.CheckboxSelectMultiple,
        choices=USER_PERMISSIONS,
        required=False
    )

    def is_valid(self):
        valid = super(NewUserForm, self).is_valid()

        if not valid:
            return False

        email = self.cleaned_data['email']
        password = self.cleaned_data['password']
        password_confirmation = self.cleaned_data['password_confirmation']

        if User.objects.filter(email=email).exists():
            self.add_error('email', 'Email já está sendo usado.')
            return False

        if not password == password_confirmation:
            self.add_error(None, 'Os dois campos de senha não combinam.')
            return False

        return True


class EditUserForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(EditUserForm, self).__init__(*args, **kwargs)
        del self.fields['password']

    USER_TYPES = [('normal', 'Normal'), ('adm', 'Administrador')]

    USER_PERMISSIONS = (
        ("manager_buildings", "Gerenciar Edifícios"),
        ("manager_transductors", "Gerenciar Transdutores"),
    )

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', ]

    username = forms.CharField(
        label="Nome de Usuário",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'username',
                'size': 30,
                'type': 'text'
            }
        )
    )

    first_name = forms.CharField(
        label="Nome",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'first_name',
                'size': 30,
                'type': 'text'
            }
        )
    )

    last_name = forms.CharField(
        label="Sobrenome",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'last_name',
                'size': 30,
                'type': 'text'
            }
        )
    )

    email = forms.CharField(
        label="E-mail",
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'name': 'email',
                'size': 30,
                'type': 'text'
            }
        )
    )

    user_type = forms.ChoiceField(
        label="Tipo de Usuário",
        choices=USER_TYPES,
        widget=forms.RadioSelect,
        initial='normal'
    )

    permissions_codenames = forms.MultipleChoiceField(
        label="Permissões",
        widget=forms.CheckboxSelectMultiple,
        choices=USER_PERMISSIONS,
        required=False
    )

    def is_valid(self):
        valid = super(EditUserForm, self).is_valid()

        if not valid:
            return False

        if User.objects.exclude(pk=self.instance.pk) \
                       .filter(email=self.cleaned_data['email']).exists():
            self.add_error('email', 'Email já está sendo usado.')
            return False

        return True


class DeleteUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = []
