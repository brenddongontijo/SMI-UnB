import mock

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import Client, TestCase
from django.utils.translation import ugettext as _

from smi_unb.buildings.models import Building
from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.transductor.models import TransductorModel, EnergyTransductor, \
                                       EnergyMeasurements
from smi_unb.report.exceptions import NoMeasurementsFoundException
from smi_unb.report.utils import GraphDateManager, GraphPlotManager


class TestReportViews(TestCase):
    def setUp(self):
        self.client = Client()

        self.superuser = User(
            username='superuser',
            first_name='super',
            last_name='user',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

        self.user = User(
            username='normaluser',
            first_name='normal',
            last_name='user',
            email="test@test.com",
            is_superuser=False,
        )
        self.user.set_password('12345')
        self.user.save()

        self.building = self.create_building()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

        self.transductor = EnergyTransductor.objects.create(
            building=self.building,
            model=self.t_model,
            name="transductor test",
            ip_address="111.111.111.111"
        )

    @mock.patch.object(
        GraphDateManager, 'get_previous_days', return_value=[1])
    def test_getting_status_code_from_pages_with_login(self, mock):
        self.client.login(username=self.superuser.email, password='12345')

        response_1 = self.client.get(reverse('report:generate_graph'))
        self.assertEqual(200, response_1.status_code)

    def test_getting_status_code_from_pages_without_login(self):
        response_1 = self.client.get(reverse('report:generate_graph'))
        self.assertEqual(302, response_1.status_code)

    @mock.patch.object(
        GraphDateManager, 'get_previous_days', return_value=[1])
    @mock.patch.object(
        GraphDateManager, 'get_graph_dates',
        return_value=['date 1', 'date 2'], autospec=True)
    @mock.patch.object(
        GraphPlotManager, 'create_line_graph',
        return_value='json test', autospec=True)
    def test_generate_graph_correct_params(
        self, mock_previous_days, mock_graph_dates, mock_line_graph
    ):
        self.create_energy_measurements()

        self.client.login(username=self.superuser.email, password='12345')

        graph_url = reverse('report:generate_graph')

        params = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': True,
            'past_measurement_day': '',
            'manual_initial_date': '',
            'manual_final_date': ''
        }

        response = self.client.post(graph_url, params)

        self.assertIn('json test', response.content.decode('utf-8'))

    @mock.patch.object(
        GraphDateManager, 'get_previous_days', return_value=[1])
    @mock.patch.object(
        GraphDateManager, 'get_graph_dates',
        return_value=['date 1', 'date 2'], autospec=True)
    @mock.patch.object(
        GraphPlotManager, 'create_line_graph',
        side_effect=NoMeasurementsFoundException('No measurements avaiable'),
        autospec=True)
    def test_generate_graph_no_measurements_found(
        self, mock_previous_days, mock_graph_dates, mock_line_graph
    ):
        self.create_energy_measurements()

        self.client.login(username=self.superuser.email, password='12345')

        graph_url = reverse('report:generate_graph')

        params = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': True,
            'past_measurement_day': '',
            'manual_initial_date': '',
            'manual_final_date': ''
        }

        response = self.client.post(graph_url, params)

        self.assertFormError(
            response,
            'form',
            None,
            _('Nenhuma medição encontrada no período informado.')
        )

    @mock.patch.object(
        GraphDateManager, 'get_previous_days', return_value=[1])
    def test_generate_graph_wrong_params(self, mock):
        self.client.login(username=self.superuser.email, password='12345')

        graph_url = reverse('report:generate_graph')

        params = {
            'measurement_type': 'voltage',
            'transductor': self.transductor.id,
            'current_measurement': '',
            'past_measurement_day': '',
            'manual_initial_date': '',
            'manual_final_date': ''
        }

        response = self.client.post(graph_url, params)

        self.assertFormError(
            response,
            'form',
            None,
            _('Nenhuma opção informada.')
        )

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building

    def create_energy_measurements(self):
        for index in range(0, 2):
            e_measurement = EnergyMeasurements()
            e_measurement.transductor = self.transductor

            e_measurement.voltage_a = 122.875 + index
            e_measurement.voltage_b = 122.784 + index
            e_measurement.voltage_c = 121.611 + index

            e_measurement.current_a = 22.831 + index
            e_measurement.current_b = 17.187 + index
            e_measurement.current_c = 3.950 + index

            e_measurement.active_power_a = 2.794 + index
            e_measurement.active_power_b = 1.972 + index
            e_measurement.active_power_c = 3.950 + index

            e_measurement.reactive_power_a = -0.251 - index
            e_measurement.reactive_power_b = -0.752 - index
            e_measurement.reactive_power_c = -1.251 - index

            e_measurement.apparent_power_a = 2.805 + index
            e_measurement.apparent_power_b = 2.110 + index
            e_measurement.apparent_power_c = 4.144 + index

            e_measurement.save()
