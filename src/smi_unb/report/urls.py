from django.conf.urls import url

from . import views

app_name = 'report'

urlpatterns = [
    url(r'^generate_graph/$', views.generate_graph, name="generate_graph"),
]
