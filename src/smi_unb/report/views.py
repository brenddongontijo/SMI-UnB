import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.translation import ugettext as _

from .exceptions import NoMeasurementsFoundException
from .forms import NewGraphForm
from .utils import GraphDateManager, GraphPlotManager


@login_required
def generate_graph(request):
    date_manager = GraphDateManager()
    past_days = date_manager.get_previous_days()
    json_data = json.dumps(None)

    if request.POST:
        form = NewGraphForm(request.POST)

        if form.is_valid():
            measurement_type = form.cleaned_data['measurement_type']
            transductor = form.cleaned_data['transductor']
            current_measurement = form.cleaned_data['current_measurement']
            previous_day = form.cleaned_data['past_measurement_day']
            manual_initial_date = form.cleaned_data['manual_initial_date']
            manual_final_date = form.cleaned_data['manual_final_date']

            initial_date, final_date = date_manager.get_graph_dates(
                current_measurement,
                previous_day,
                manual_initial_date,
                manual_final_date
            )

            try:
                plot_manager = GraphPlotManager()

                json_data = plot_manager.create_line_graph(
                    transductor,
                    measurement_type,
                    initial_date,
                    final_date
                )
            except NoMeasurementsFoundException:
                form.add_error(
                    None,
                    _(
                        ('Nenhuma medição encontrada no' +
                         ' período informado.')
                    )
                )
    else:
        form = NewGraphForm()

    return render(
        request,
        'graphics/generate_graph.html',
        {
            'form': form,
            'json': json_data,
            'past_days': past_days,
        }
    )
