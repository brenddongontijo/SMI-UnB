from django.conf.urls import url

from . import views

app_name = 'authentication'

urlpatterns = [
    url(r'^$', views.make_login, name='make_login'),
    url(r'^logout/$', views.make_logout, name='make_logout'),
]
