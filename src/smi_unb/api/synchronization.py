import json
import requests

from django.core.urlresolvers import reverse

from smi_unb.buildings.models import Building
from .serializers import EnergyTransductorSerializer, BuildingSerializer, \
                         EnergyMeasurementsSerializer


class SyncManager(object):

    @classmethod
    def sync_building(cls, building, new=False):
        serializer = BuildingSerializer

        slave_ip = building.server_ip_address

        synchronizer_class = InstanceSynchronizer(
            slave_ip, building, serializer)

        synchronized = synchronizer_class.sync(new)

        return synchronized

    @classmethod
    def sync_transductor(cls, transductor, new=False):
        serializer = EnergyTransductorSerializer

        slave_ip = transductor.building.server_ip_address

        synchronizer_class = InstanceSynchronizer(
            slave_ip, transductor, serializer)

        synchronized = synchronizer_class.sync(new)

        return synchronized


class InstanceSynchronizer(object):
    def __init__(self, slave_ip, instance, serializer):
        self.slave_ip = slave_ip
        self.instance = instance
        self.serializer = serializer
        self.api_view_set = self.instance.api_view_set()

    def sync(self, new=False):
        instance_synchronized = False

        json_data = SyncUtils.create_json_data(self.serializer, self.instance)

        endpoint_url = SyncUtils.generate_endpoint_url(
            self.slave_ip, new, self.api_view_set, str(self.instance.id))

        if new:
            request = SyncUtils.send_post_request(endpoint_url, json_data)
        else:
            request = SyncUtils.send_patch_request(endpoint_url, json_data)

        if request.status_code is 201:
            instance_synchronized = True

        return instance_synchronized


class SyncUtils(object):
    @classmethod
    def generate_endpoint_url(
        cls, slave_ip, new, api_view_set,
        instance_id='', query_params='', port='8000'
    ):
        endpoint_url = ''

        if new:
            endpoint_url = reverse(api_view_set + '-list')
        else:
            endpoint_url = reverse(
                api_view_set + '-detail', args=(instance_id, ))

        url = 'http://' + slave_ip + ':' \
                        + port + endpoint_url + query_params

        return url

    @classmethod
    def send_post_request(cls, url, json_data):
        headers = {'content-type': 'application/json'}

        return requests.post(url, data=json_data, headers=headers)

    @classmethod
    def send_patch_request(cls, url, json_data):
        headers = {'content-type': 'application/json'}

        return requests.patch(url, data=json_data, headers=headers)

    @classmethod
    def get_request_data(cls, url):
        data = None

        request = requests.get(url)

        if request.status_code is 200:
            data = request.json()

        return data

    @classmethod
    def create_json_data(cls, serializer_class, instance, many=False):
        object_serialized = serializer_class(instance=instance, many=many)

        return json.dumps(object_serialized.data)


class EnergyMeasurementSynchronizer(object):
    def __init__(self):
        self.buildings = Building.objects.filter(active=True)

    def perform_all_measurements_sync(self):
        query_params = '?last=true'

        for building in self.buildings:
            url = SyncUtils.generate_endpoint_url(
                building.server_ip_address, True,
                'energymeasurements', query_params=query_params
            )

            request_data = SyncUtils.get_request_data(url)
            measurements = request_data['results']

            if measurements:
                energy_serializer = EnergyMeasurementsSerializer(
                    data=measurements, many=True)

                if energy_serializer.is_valid():
                    energy_serializer.save()
                    self.update_transductors_slave_last_sent(
                        building, measurements)

    def update_transductors_slave_last_sent(self, building, measurements):
        # Removing duplicate transductors ids in measurements
        id_transductors_to_update = list(
            set([measurement['transductor'] for measurement in measurements]))

        updated_transductors = building.update_transductors_sent(
            id_transductors_to_update)

        url = SyncUtils.generate_endpoint_url(
            building.server_ip_address, True,
            'energytransductor', query_params='?update_collection=true'
        )

        json_data = SyncUtils.create_json_data(
            EnergyTransductorSerializer, updated_transductors, many=True)

        request = SyncUtils.send_post_request(url, json_data)

        if request.status_code is 200:
            # success
            pass
        else:
            # exception?
            pass
