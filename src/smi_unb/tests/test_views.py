from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class SMITest(TestCase):
    def setUp(self):
        self.superuser = User(
            username='superuser',
            first_name='super',
            last_name='user',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

    def test_getting_status_code_from_pages_with_login(self):
        self.client.login(username=self.superuser.email, password='12345')

        response_1 = self.client.get(reverse('dashboard'))
        self.assertEqual(200, response_1.status_code)

    def test_getting_status_code_from_pages_without_login(self):
        response_1 = self.client.get(reverse('dashboard'))
        self.assertEqual(302, response_1.status_code)

        response_2 = self.client.get('')
        self.assertEqual(200, response_2.status_code)
        self.assertTemplateUsed(response_2, 'index.html')
